"""hw_project01 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.conf.urls import url
from book_api import views as book_api
from index import views as web_index


urlpatterns = [
    path('admin/', admin.site.urls),
    path('',web_index.web_index,name='index'),
    url(r'^book_api/$',book_api.BookList.as_view(),name='book_api_by_all'),
    url(r'^book_api/(?P<pk>[0-9]+)/$', book_api.BookDetail.as_view(),name='book_api_by_id'),
]

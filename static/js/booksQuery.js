$("#all_books_submit").click(function() {
    ajax_url = "http://18.225.11.33:8000/book_api/"
    $.ajax({
        type :"GET",
        url  : ajax_url,
        dataType: "json",
        success: function (data) {
            console.log("Getting all books information successfully!")
            console.log(data)
        }
    })
});

$("#single_book_submit").click(function() {
    var book_ID = $("#book_ID").val();
    ajax_url = "http://18.225.11.33:8000/book_api/".concat(book_ID,"/")

    $.ajax({
        type :"GET",
        url  : ajax_url,
        dataType: "json",
        success: function (data) {
            console.log("Getting particulat book information by book_ID successfully!")
            console.log(data)
        },
        error: function(data){
            alert("you have to enter the ID of book.")
        }
    })
});

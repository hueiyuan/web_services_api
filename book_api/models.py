# -*- coding: UTF-8 -*-
import datetime
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from .fields import BigIntegerAutoField,YearField

class Book(models.Model):
    id = BigIntegerAutoField(primary_key=True)
    title = models.CharField(max_length=255,unique=True,null=False)
    author = models.CharField(max_length=255,null=False)
    isbn = models.CharField(max_length=16,null=False)
    publisher = models.CharField(max_length=255,null=False)
    publication_year = YearField(null=False)
    last_modified_date = models.DateTimeField(null=False)
    # created_date = models.DateTimeField(null=False)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'book'


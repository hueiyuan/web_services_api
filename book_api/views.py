# -*- coding: UTF-8 -*-
from __future__ import unicode_literals
import os
import sys
import json
from django.shortcuts import render,redirect
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.template.loader import get_template

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import BookSerializer
from .models import Book

class BookDetail(APIView):
    def get_object(self,pk):
        try:
            return Book.objects.get(id__exact=pk)
        except Book.DoesNotExist as e:
            raise Http404

    def get(self,request,pk,format=None):
        book = self.get_object(pk)
        serializer = BookSerializer(book)
        data = serializer.data

        json_response_format = {
            'error':False,
            "message":"",
            "book":data
        }
        return Response(json_response_format)

    def put(self,request,pk,format=None):
        book = self.get_object(pk)
        serializer = BookSerializer(book,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_200_OK)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,pk,format=None):
        book = self.get_object(pk)
        book.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class BookList(APIView):
    def get(self,request,format=None):
        try:
            serializer = BookSerializer(Book.objects.all(),many=True)
            data = serializer.data
        except Book.DoesNotExist as e:
            raise Http404

        json_response_format = {
            'error':False,
            "message":"",
            "books":data
        }

        return Response(json_response_format,status=status.HTTP_200_OK)

    def post(self,request=None,format=None):
        serializer = BookSerializer(request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_200_OK)

        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
